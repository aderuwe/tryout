<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmotionalState;
use AppBundle\Form\EmotionalStateType;
use AppBundle\Service\EmotionalStateManager;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteResource("state", pluralize=false)
 */
class EmotionalStateController extends FOSRestController implements ClassResourceInterface
{
    public function getAction($id)
    {
        $entityManager = $this->get('doctrine.orm.default_entity_manager');
        $repository = $entityManager->getRepository('AppBundle:EmotionalState');
        $state = $repository->find($id);

        if (null !== $state) {
            $view = View::create($state);
        } else {
            $view = View::create(null, Response::HTTP_OK);
        }

        return $this->handleView($view);
    }

    public function postAction(Request $request)
    {
        $state = new EmotionalState();

        $form = $this->createForm(EmotionalStateType::class, $state);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var EmotionalStateManager $manager */
            $manager = $this->get(EmotionalStateManager::class);
            $manager->create($state);

            $view = View::createRouteRedirect('get_state', ['id' => $state->getId()]);
        } else {
            $view = View::create($form->getErrors(true), Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    public function cgetAction(Request $request)
    {
        $page = $request->query->get('page', 1);

        /** @var EmotionalStateManager $manager */
        $manager = $this->get(EmotionalStateManager::class);
        $pager = $manager->getPager($page);

        $states = $pager->getCurrentPageResults();

        $view = View::create(['total' => $pager->getNbResults(), 'count' => count($states), 'states' => $states]);

        return $this->handleView($view);
    }
}
