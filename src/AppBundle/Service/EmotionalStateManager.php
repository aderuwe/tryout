<?php

namespace AppBundle\Service;

use AppBundle\Entity\EmotionalState;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class EmotionalStateManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param EmotionalState $state
     */
    public function create(EmotionalState $state)
    {
        $this->entityManager->persist($state);
        $this->entityManager->flush();
    }

    /**
     * @param integer $page
     *
     * @return Pagerfanta
     */
    public function getPager($page)
    {
        $repository = $this->entityManager->getRepository('AppBundle:EmotionalState');

        $queryBuilder = $repository->createQueryBuilder('es')
            ->orderBy('es.id');

        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(5);
        $pager->setCurrentPage($page);

        return $pager;
    }
}
