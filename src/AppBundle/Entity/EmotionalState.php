<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="emotional_state")
 */
class EmotionalState
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"export"})
     *
     * @var integer
     */
    private $userId;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     * @Serializer\Groups({"export"})
     *
     * @var boolean
     */
    private $happy;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"export"})
     *
     * @var DateTime
     */
    private $createdAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param integer $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return boolean
     */
    public function getHappy()
    {
        return $this->happy;
    }

    /**
     * @param boolean $happy
     */
    public function setHappy($happy)
    {
        $this->happy = $happy;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
